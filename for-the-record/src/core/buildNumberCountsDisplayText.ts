import { NumberCount } from ".";

export const buildNumberCountsDisplayText = (
  numberCounts: ReadonlyArray<NumberCount>
) => {
  const countsString = numberCounts
    .slice()
    // Order the number counts descending by the count
    .sort((a, b) => b.count - a.count)
    .map(({ number, count }) => `${number}:${count}`)
    .join(", ");

  return countsString;
};

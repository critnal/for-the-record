import { isOneOfFirstFibonacci1000Numbers } from "./isOneOfFirstFibonacci1000Numbers";

describe.each([
  [0n, true],
  [1n, true],
  [2n, true],
  [3n, true],
  [4n, false],
  [5n, true],
  [6n, false],
  [7n, false],
  [8n, true],
  [9n, false],

  // 100th fibonacci number
  [354224848179261915075n, true],

  // 1000th fibonacci number
  [
    26863810024485359386146727202142923967616609318986952340123175997617981700247881689338369654483356564191827856161443356312976673642210350324634850410377680367334151172899169723197082763985615764450078474174626n,
    true,
  ],

  // The 1001st fibonacci number
  [
    43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875n,
    false,
  ],
])("isOneOfFibonacciFirst1000Numbers(%o, %o)", (number, expected) => {
  test(`returns ${expected}`, () => {
    const result = isOneOfFirstFibonacci1000Numbers(number);
    expect(result).toBe(expected);
  });
});

export type NumberCount = Readonly<{
  /**
   * We need to handle numbers as bigints because we need to support arbitrarily large numbers, up to
   * and beyond the 1000th fibonacci number, which is 2x10^208, which is much bigger than the normal
   * number limit of 2^53
   */
  number: bigint;
  count: number;
}>;

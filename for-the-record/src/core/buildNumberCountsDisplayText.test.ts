import { buildNumberCountsDisplayText } from "./buildNumberCountsDisplayText";

test(`Handles empty list`, () => {
  const result = buildNumberCountsDisplayText([]);
  expect(result).toStrictEqual("");
});

test(`Single number count`, () => {
  const result = buildNumberCountsDisplayText([{ number: 1n, count: 1 }]);
  expect(result).toStrictEqual("1:1");
});

test(`Multiple number counts`, () => {
  const result = buildNumberCountsDisplayText([
    { number: 1n, count: 1 },
    { number: 2n, count: 1 },
  ]);
  expect(result).toStrictEqual("1:1, 2:1");
});

test(`Orders descending by count`, () => {
  const result = buildNumberCountsDisplayText([
    { number: 1n, count: 1 },
    { number: 2n, count: 2 },
  ]);
  expect(result).toStrictEqual("2:2, 1:1");
});

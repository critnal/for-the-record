/**
 * Checks if a number is one of the first 1000 fibonacci numbers.
 * The fibonacci sequence can start at 0 or 1. For this application we start at 0.
 * This might not be the optimal algorithm, but it does the job with no noticeable performance
 * issues for the user.
 * Having to support the first 1000 fibonacci numbers raised some technical concerns
 *  1. We avoided using recursion because adding 1000 to the stack is bound to produce problems. Apparently
 *     browsers can support stack limits in the tens of thousands, but it's better to just remove that potential
 *     issue.
 *  2. The numbers get much bigger than a normal javascript number can support, so bigint was used
 *  3. There are equations that can be used to check if a number is a fibonacci number, but those weren't used
 *     because apparently they can lose accuracy when numbers get very big.
 */
export const isOneOfFirstFibonacci1000Numbers = (number: bigint): boolean => {
  if (number === 0n || number === 1n) {
    return true;
  }

  if (number > fib1000) {
    return false;
  }

  let i = 0;
  for (const fibonacciNumber of generateFibonacciSequence()) {
    if (number === fibonacciNumber) {
      return true;
    }

    if (fibonacciNumber > number) {
      return false;
    }

    if (i >= 1000) {
      break;
    }

    i++;
  }

  return false;
};

/**
 * This is the 1000th fibonacci number based on the sequence 0, 1, 1, 2, ...
 */
const fib1000 = 26863810024485359386146727202142923967616609318986952340123175997617981700247881689338369654483356564191827856161443356312976673642210350324634850410377680367334151172899169723197082763985615764450078474174626n;

function* generateFibonacciSequence() {
  let [a, b] = [0n, 1n];

  yield a;
  yield b;

  while (true) {
    [a, b] = [b, a + b];
    yield b;
  }
}

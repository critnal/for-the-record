/**
 * There doesn't appear to be a native function for parsing a `string` into a `bigint` that is safe.
 * The constructor `BigInt()` throws a runtime error for some values, and there's no `parseBigInt()` function.
 * This function allows consumers to handle the possible cases more safely.
 */
export const parseBigInt = (stringValue: string): null | bigint => {
  try {
    return BigInt(stringValue);
  } catch {
    return null;
  }
};

import { parseBigInt } from "./parseBigInt";

describe.each([
  ["a", null],
  ["", 0n],
  ["0", 0n],
  ["1", 1n],
  ["-1", -1n],
  // One number greater than the max number value, to make sure that the function isn't restricted to
  // the limits of the number type for any reason
  ["9007199254740992", 9007199254740992n],
])("parseBigInt(%o, %o)", (stringValue, expected) => {
  test(`returns ${expected}`, () => {
    const result = parseBigInt(stringValue);
    expect(result).toStrictEqual(expected);
  });
});

export const addMessage = ({
  currentMessages,
  message,
}: {
  currentMessages: ReadonlyArray<string>;
  message: string;
}) => {
  return (
    [...currentMessages, message]
      // Limit to last 10 messages
      .reverse()
      .slice(0, 10)
      .reverse()
  );
};

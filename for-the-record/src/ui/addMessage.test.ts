import { addMessage } from "./addMessage";

test(`Add message to empty list`, () => {
  const result = addMessage({ currentMessages: [], message: "1" });
  expect(result).toStrictEqual(["1"]);
});

test(`Add message to non-empty list`, () => {
  const result = addMessage({ currentMessages: ["1"], message: "2" });
  expect(result).toStrictEqual(["1", "2"]);
});

test(`Retain last 10 messages in order`, () => {
  const twoToTen = ["2", "3", "4", "5", "6", "7", "8", "9", "10"];
  const result = addMessage({
    currentMessages: ["1", ...twoToTen],
    message: "11",
  });
  expect(result).toStrictEqual([...twoToTen, "11"]);
});

import React from "react";

/**
 * This hook sets up a timer that will trigger the supplied callback every `timeIntervalSeconds`.
 * The timer can be paused. When it is resumed, it will resume with the same amount of time that
 * was remaining in the interval when it was paused.
 */
export const usePausableTimer = ({
  timeIntervalSeconds,
  onTimeIntervalElapsed,
}: {
  timeIntervalSeconds: null | number;
  onTimeIntervalElapsed: () => void;
}) => {
  const lastTimeIntervalOccurredAt = React.useRef<null | Date>(null);

  const [paused, setPaused] = React.useState<boolean>(false);

  /**
   * `pausedAt` is used to track when the timer was paused, so that when it's resumed we can
   * work out how much time was remaining in the interval.
   */
  const pausedAt = React.useRef<null | Date>(null);

  React.useEffect(() => {
    // The purpose of this effect is to ensure the timer is reset to unpaused whenever the application
    // is reset, which is when the time interval is cleared
    setPaused(false);
  }, [timeIntervalSeconds]);

  React.useEffect(() => {
    if (timeIntervalSeconds == null || paused) {
      return;
    }

    const timeRemainingForCurrentIntervalMs = (() => {
      const timeIntervalMs = timeIntervalSeconds * 1000;

      if (lastTimeIntervalOccurredAt.current == null) {
        return timeIntervalMs;
      }

      const timeToResumeFrom = pausedAt.current ?? new Date();
      pausedAt.current = null;

      const lastIntervalElapsedMs =
        timeToResumeFrom.getTime() -
        lastTimeIntervalOccurredAt.current.getTime();
      return timeIntervalMs - lastIntervalElapsedMs;
    })();

    const timer = setTimeout(() => {
      onTimeIntervalElapsed();

      lastTimeIntervalOccurredAt.current = new Date();
    }, timeRemainingForCurrentIntervalMs);

    return () => {
      clearTimeout(timer);
    };
  }, [onTimeIntervalElapsed, paused, timeIntervalSeconds]);

  const pause = React.useCallback(() => {
    setPaused(true);
    pausedAt.current = new Date();
  }, []);

  const resume = React.useCallback(() => {
    setPaused(false);
  }, []);

  return { paused, pause, resume };
};

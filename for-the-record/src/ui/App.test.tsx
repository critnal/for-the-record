import { render, screen, fireEvent, act } from "@testing-library/react";
import { App } from "./App";

beforeEach(() => {
  jest.useFakeTimers();
});

afterEach(() => {
  jest.runOnlyPendingTimers();
  jest.useRealTimers();
});

const clickButton = (button: string) => {
  fireEvent.click(screen.getByRole("button", { name: button }));
};

const submitTextInput = (text: string) => {
  fireEvent.change(screen.getByRole("textbox"), { target: { value: text } });
  clickButton("Submit");
};

const completeTimeIntervalForm = () => {
  // Submit a 1 second interval
  submitTextInput("1");
};

const submitNumber = (number: number) => {
  submitTextInput(number.toString());
};

test("Show time interval form initially", () => {
  render(<App />);

  expect(
    screen.getByText(
      "Please input the amount of time in seconds between emitting numbers and their frequency"
    )
  ).toBeInTheDocument();

  expect(screen.getByRole("textbox")).toBeInTheDocument();

  expect(screen.getByRole("button")).toBeInTheDocument();
});

test("Can complete time interval form", () => {
  render(<App />);
  submitTextInput("1");
  expect(screen.getByText("Please enter the first number")).toBeInTheDocument();
});

test("Enter a fibonacci number", async () => {
  render(<App />);
  completeTimeIntervalForm();
  submitNumber(1);

  act(() => {
    jest.runOnlyPendingTimers();
  });

  expect(await screen.findByText("FIB")).toBeInTheDocument();
  expect(await screen.findByText("1:1")).toBeInTheDocument();
});

test("Enter a non-fibonacci number", async () => {
  render(<App />);
  completeTimeIntervalForm();
  submitNumber(4);

  act(() => {
    jest.runOnlyPendingTimers();
  });

  expect(screen.queryByText("FIB")).not.toBeInTheDocument();
  expect(await screen.findByText("4:1")).toBeInTheDocument();
});

test("Ensure numbers are counted separately", async () => {
  render(<App />);
  completeTimeIntervalForm();
  submitNumber(1);
  submitNumber(1);
  submitNumber(2);

  act(() => {
    jest.runOnlyPendingTimers();
  });

  expect(await screen.findByText("1:2, 2:1")).toBeInTheDocument();
});

test("Can pause and resume timer", async () => {
  render(<App />);
  completeTimeIntervalForm();
  submitNumber(1);

  // Time interval was set to 1000ms

  act(() => {
    jest.advanceTimersByTime(990);
  });

  // Pause after 990ms
  clickButton("Pause");

  // Another 1000ms passes
  act(() => {
    jest.advanceTimersByTime(1000);
  });

  // Expect the counts to not have been displayed yet
  expect(screen.queryByText("1:1")).not.toBeInTheDocument();

  // Resume
  clickButton("Resume");

  // The timer should elapse after the amount of time that was remaining in the previous interval
  // from when it was paused
  act(() => {
    jest.advanceTimersByTime(10);
  });

  // The counts should now be displayed
  expect(await screen.findByText("1:1")).toBeInTheDocument();
});

test("Can quit", async () => {
  render(<App />);
  completeTimeIntervalForm();
  submitNumber(1);

  act(() => {
    jest.runOnlyPendingTimers();
  });

  expect(await screen.findByText("1:1")).toBeInTheDocument();

  clickButton("Quit");

  // Shows exit messages
  expect(await screen.findByText("1:1")).toBeInTheDocument();
  expect(
    await screen.findByText("Thanks for playing, press the button to exit.")
  ).toBeInTheDocument();

  clickButton("Quit");

  // Shows the initial UI again
  expect(
    await screen.findByText(
      "Please input the amount of time in seconds between emitting numbers and their frequency"
    )
  ).toBeInTheDocument();
});

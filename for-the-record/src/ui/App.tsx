import React from "react";
import {
  NumberCount,
  isOneOfFirstFibonacci1000Numbers,
  parseBigInt,
} from "../core";
import { addMessage } from "./addMessage";
import { usePausableTimer } from "./usePausableTimer";
import { buildNumberCountsDisplayText } from "../core";

export const App = () => {
  const [timeIntervalSeconds, setTimeIntervalSeconds] = React.useState<
    number | null
  >(null);

  const [numberCounts, setNumberCounts] = React.useState<
    ReadonlyArray<NumberCount>
  >([]);

  const [messages, setMessages] = React.useState<ReadonlyArray<string>>([]);
  const [quitting, setQuitting] = React.useState<boolean>(false);

  const { paused, pause, resume } = usePausableTimer({
    timeIntervalSeconds,
    onTimeIntervalElapsed: () => {
      const numberCountsMessage = buildNumberCountsDisplayText(numberCounts);
      setMessages((messages) =>
        addMessage({ currentMessages: messages, message: numberCountsMessage })
      );
    },
  });

  if (timeIntervalSeconds == null) {
    return (
      <NumberInput
        text="Please input the amount of time in seconds between emitting numbers and their frequency"
        onSubmit={(text) => {
          const parsedNumber = parseFloat(text);
          if (!isNaN(parsedNumber)) {
            setTimeIntervalSeconds(parsedNumber);
          } else {
            alert("Please enter a valid number");
          }
        }}
      />
    );
  }

  if (quitting) {
    const confirmQuit = () => {
      setTimeIntervalSeconds(null);
      setNumberCounts([]);
      setMessages([]);
      setQuitting(false);
    };

    return (
      <div>
        <div>{buildNumberCountsDisplayText(numberCounts)}</div>
        <div>Thanks for playing, press the button to exit.</div>
        <button onClick={confirmQuit}>Quit</button>
      </div>
    );
  }

  const inputText =
    numberCounts.length < 1
      ? "Please enter the first number"
      : "Please enter the next number";

  const onSubmitNumber = (text: string) => {
    const bigInt: null | bigint = parseBigInt(text);

    if (bigInt == null) {
      alert("Please enter a valid number");
      return;
    }

    const updatedNumberCounts = addNumberToCounts(numberCounts, bigInt);
    setNumberCounts(updatedNumberCounts);

    if (isOneOfFirstFibonacci1000Numbers(bigInt)) {
      setMessages((messages) =>
        addMessage({ currentMessages: messages, message: "FIB" })
      );
    }
  };

  const quit = () => {
    setQuitting(true);
    pause();
  };

  return (
    <div
      style={{
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
      }}
    >
      <div style={{ flex: "1 1 auto", backgroundColor: "grey" }}>
        <MessagesDisplay messages={messages} />
      </div>
      <div style={{ flex: "0 0 100px", backgroundColor: "honeydew" }}>
        <NumberInput text={inputText} onSubmit={onSubmitNumber} />
        <div>
          <button onClick={pause} disabled={paused}>
            Pause
          </button>
          <button onClick={resume} disabled={!paused}>
            Resume
          </button>
          <button onClick={quit}>Quit</button>
        </div>
      </div>
    </div>
  );
};

const NumberInput = ({
  text,
  onSubmit,
}: {
  text: string;
  onSubmit: (text: string) => void;
}) => {
  const numberInputValueRef = React.useRef<string>("");

  return (
    <div>
      {text}

      <input
        type="text"
        onChange={(event) => {
          numberInputValueRef.current = event.currentTarget.value;
        }}
      />

      <button
        onClick={() => {
          onSubmit(numberInputValueRef.current);
        }}
      >
        Submit
      </button>
    </div>
  );
};

const addNumberToCounts = (
  numberCounts: ReadonlyArray<NumberCount>,
  number: bigint
): ReadonlyArray<NumberCount> => {
  const currentCount =
    numberCounts.find((nc) => nc.number === number)?.count ?? 0;

  const updatedCount: NumberCount = {
    number,
    count: currentCount + 1,
  };

  const updatedNumberCounts = numberCounts
    .filter((nc) => nc.number !== updatedCount.number)
    .concat(updatedCount);

  return updatedNumberCounts;
};

const MessagesDisplay = ({ messages }: { messages: ReadonlyArray<string> }) => {
  return (
    <>
      {messages.map((message, i) => (
        <div key={`${i}-${message}`}>{message}</div>
      ))}
    </>
  );
};
